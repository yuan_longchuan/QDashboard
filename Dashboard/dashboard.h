#ifndef PANEL_H
#define PANEL_H

#include <QWidget>
#include <QTimer>
#include <QPropertyAnimation>

QT_BEGIN_NAMESPACE
namespace Ui { class Dashboard; }
QT_END_NAMESPACE

class Dashboard : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(double value READ value WRITE setValue)
public:
	Dashboard(QWidget *parent = nullptr);
    Dashboard(int minValue, int tickValue, int tickCount, int value, QString text, QWidget *parent = nullptr);
    ~Dashboard();

    void set(int minValue, int tickValue, int tickCount, int value, QString text);
public slots:
    void updateValue(int value);
    void setText(QString text);
private:
    int value();
    void setValue(double value);
protected:
    void paintEvent(QPaintEvent *event) override;

    // 画指针
    virtual void drawArrow(QPainter& painter);
private:
    // 计算value值对应的角度
    double valueToAngle(double value);
private:
    Ui::Dashboard *ui;

    // 最小刻度
	int minValue{ 0 };

    // 每个大刻度的范围
	int tickValue{ 10 };

    // 多少个大刻度
    int tickCount{ 10 };

    // 仪表盘当前值
    double currentValue{ 0 };

    // 仪表盘字符
	QString text{ "panel" };

    // 控制指针旋转
    QPropertyAnimation animation;
};
#endif // PANEL_H
