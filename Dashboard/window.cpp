#include "window.h"
#include "ui_window.h"
#include "dashboard.h"

Window::Window(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Window)
{
    ui->setupUi(this);
    int minValue = -50;
    int tickValue = 10;
    int tickCount = 11;
    int maxValue = minValue + tickValue*(tickCount-1);
    int currentValue = 0;
    Dashboard *w = new Dashboard(minValue,tickValue,tickCount,currentValue,"Board");
    ui->widget->layout()->addWidget(w);
    ui->spinBox->setRange(minValue,maxValue);
    ui->spinBox->setValue(currentValue);
    ui->spinBox->valueChanged(currentValue);
    ui->horizontalSlider->setRange(minValue,maxValue);
    connect(ui->spinBox, SIGNAL(valueChanged(int)), w, SLOT(updateValue(int)));
    connect(ui->horizontalSlider, SIGNAL(sliderMoved(int)), w, SLOT(updateValue(int)));

}

Window::~Window()
{
    delete ui;
}
